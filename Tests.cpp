#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "calculator.h"
using namespace std;

using testing::Eq;

namespace
{
    calculator calculatorObj;

    TEST(calculator, add)
    {
        ASSERT_EQ(100, calculatorObj.add(50,50));
        ASSERT_EQ(-100, calculatorObj.add(-50,-50));
        ASSERT_EQ(0, calculatorObj.add(-50,50));

        ASSERT_EQ(100.0, calculatorObj.add(50.1,49.9));
        ASSERT_EQ(-100.0, calculatorObj.add(-50.1,-49.9));
        ASSERT_EQ(0.0, calculatorObj.add(-50.1,50.1));
    }

    TEST(calculator, subtract)
    {
        ASSERT_EQ(0, calculatorObj.subtract(50,50));
        ASSERT_EQ(0, calculatorObj.subtract(-50,-50));
        ASSERT_EQ(20, calculatorObj.subtract(40,20));
        ASSERT_EQ(-100, calculatorObj.subtract(-50,50));

        ASSERT_NEAR(0.2, calculatorObj.subtract(50.1, 49.9), 0.01);
        ASSERT_NEAR(-0.2, calculatorObj.subtract(-50.1,-49.9), 0.01);
        ASSERT_NEAR(0.0, calculatorObj.subtract(50.1,50.1), 0.01);
    }

    TEST(calculator, multiply)
    {
        ASSERT_EQ(2500, calculatorObj.multiply(50,50));
        ASSERT_EQ(2500, calculatorObj.multiply(-50,-50));
        ASSERT_EQ(0, calculatorObj.multiply(40,0));
        ASSERT_EQ(-2500, calculatorObj.multiply(-50,50));

        ASSERT_NEAR(0.25, calculatorObj.multiply(0.5, 0.5), 0.01);
        ASSERT_NEAR(-0.25, calculatorObj.multiply(-0.5, 0.5), 0.01);
        ASSERT_NEAR(0, calculatorObj.multiply(0.5, 0.0), 0.01);
    }

    TEST(calculator, square)
    {
        ASSERT_EQ(2500, calculatorObj.square(50));
        ASSERT_EQ(2500, calculatorObj.square(-50));
        ASSERT_EQ(0, calculatorObj.square(0));

        ASSERT_NEAR(0.25, calculatorObj.square(0.5), 0.01);
        ASSERT_NEAR(0.25, calculatorObj.square(-0.5), 0.01);
        ASSERT_NEAR(0, calculatorObj.square(0.0), 0.01);
    }



}

