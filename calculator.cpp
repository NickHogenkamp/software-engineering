//
// Created by hogen on 13-10-2018.
//
#include <iostream>
#include "calculator.h"
using namespace std;


int calculator::add(int a, int b) {
    return (a + b);
}

int calculator::subtract(int a, int b){
    return (a - b);
}

int calculator::multiply(int a, int b){
    return (a * b);
}

int calculator::divide(int a, int b){
    try{
        if (b == 0){
            throw 1;
        }
    }catch(int x){
        cout << "cant devide by 0" << endl;
        return 0;
    }

    return (a / b);
}

int calculator::square(int a){
    return (a * a);
}

double calculator::add(double a, double b){
    return (a + b);
}

double calculator::subtract(double a, double b){
    return (a - b);
}

double calculator::multiply(double a, double b){
    return (a * b);
}

double calculator::divide(double a, double b){
    try{
        if (b == 0.0){
            throw 1;
        }
    }catch(double x){
        cout << "cant devide by 0" << endl;
        return 0;
    }

    return (a / b);
}

double calculator::square(double a){
    return (a * a);
}