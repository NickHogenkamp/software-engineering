//
// Created by hogen on 13-10-2018.
//

#ifndef OPDRACHT2_CALCULATOR_H
#define OPDRACHT2_CALCULATOR_H


class calculator {

    public:
        int add(int a, int b);
        int subtract(int a, int b);
        int multiply(int a, int b);
        int divide(int a, int b);
        int square(int a);

        double add(double a, double b);
        double subtract(double a, double b);
        double multiply(double a, double b);
        double divide(double a, double b);
        double square(double a);
    private:

};


#endif //OPDRACHT2_CALCULATOR_H
